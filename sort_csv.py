import csv
import operator

def cmp(a):
        try:
            return int(a[3])
        except TypeError:
            return a
with open("words_popular.csv", "r") as table, open("sorted_words.csv", 'w') as n_table:
    reader = csv.reader(table, delimiter=',')
    writer = csv.writer(n_table, delimiter=',')
    writer.writerow(next(reader))
    sor = sorted(reader, key=cmp, reverse=True)
    for row in sor:
        try:
            if (float(row[4])/float(row[3]) <= 0.3):
                writer.writerow(row)
        except ValueError:
            pass

