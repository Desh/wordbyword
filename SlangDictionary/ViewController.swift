//
//  ViewController.swift
//  SlangDictionary
//
//  Created by Денис Шаклеин on 21/11/2016.
//  Copyright © 2016 desage. All rights reserved.
//

import UIKit
import CoreData
import Firebase


class ViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var dateComponent = DateComponents()
    
    
    let cellId = "cellId"
    var slangWords = [SlangWord]()
    var managedObjextContext:NSManagedObjectContext!
    var cellCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateComponent.year = 2017
        dateComponent.month = 5
        dateComponent.day = 9
        let calendar = NSCalendar.current
        let start = calendar.date(from: dateComponent)
        let components = calendar.dateComponents([.day], from: start!, to: Date())
        cellCount = components.day!
        setupCollectionView()
        managedObjextContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
//        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Avenir-Heavy", size: 21)!]
//        let image = UIImage(named: "slang")!
//        let imv = UIImageView(image: image)
//        navigationItem.titleView = imv
        loadData()
        
        
    }
    
    func setupCollectionView() {
        collectionView?.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        collectionView?.isPagingEnabled = true
        
    }
    func loadData(){
        let wordRequest:NSFetchRequest<SlangWord> = SlangWord.fetchRequest()
        
        do {
            slangWords  = try managedObjextContext.fetch(wordRequest)
            self.collectionView?.reloadData()
        }catch {
            print("Could not load data from database \(error.localizedDescription)")
        }
    }

    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.cellCount
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    // cell inter spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DayWordCollectionViewCell
        
        
        cell.wordLabel.text = slangWords[self.cellCount - indexPath.row].word
        cell.wordTextView.text = slangWords[self.cellCount - indexPath.row].definition
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let calendar = NSCalendar.current
        let start = calendar.date(from: dateComponent)
        
        var daysfromStart: Date {
            return (Calendar.current as NSCalendar).date(byAdding: .day, value: self.cellCount - indexPath.row, to: start!, options: [])!
        }
        
        let dateString = dateFormatter.string(from:daysfromStart)
        cell.timeLabel.text = dateString
        return cell
    }
}

