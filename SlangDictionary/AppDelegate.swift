//
//  AppDelegate.swift
//  SlangDictionary
//
//  Created by Денис Шаклеин on 21/11/2016.
//  Copyright © 2016 desage. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        FirebaseApp.configure()
        let authenticatedUser = (UserDefaults.standard.object(forKey: "user") != nil)
//        print()
        if !authenticatedUser {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.makeKeyAndVisible()
            window?.rootViewController = UINavigationController(rootViewController: LoginController())

        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let MainController = storyboard.instantiateViewController(withIdentifier: "Main") as!TabBarController
            
            window?.rootViewController = MainController
            
        }
        managedObjextContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        if (UserDefaults.standard.string(forKey: "slangwords") == nil) {
            UserDefaults.standard.set("stored", forKey: "slangwords")
            parseJSON()
        }
//        UISearchBar.appearance().barTintColor = UIColor.candyGreen()
//        UISearchBar.appearance().tintColor = UIColor.white
//        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = UIColor.candyGreen()
        return true
    }
    var managedObjextContext:NSManagedObjectContext!
    
    func parseJSON() {
        let path : String = Bundle.main.path(forResource: "words", ofType: "json") as String!
        let jsonData  = NSData(contentsOfFile: path) as Data!
        let readableJSON = JSON(data: jsonData!, options: JSONSerialization.ReadingOptions.mutableContainers, error: nil)
        
        let NumberofRows = readableJSON.count
        for i in 0...NumberofRows - 1 {
            let word = SlangWord(context: managedObjextContext)
            word.word = readableJSON[i]["word"].string!
            word.definition = readableJSON[i]["definition"].string!
            word.example = readableJSON[i]["example"].string!
            word.thumbs_up = Int32(readableJSON[i]["thumbs_up"].int!)
            word.thumbs_down = Int32(readableJSON[i]["thumbs_down"].int!)
            word.defid = Int32(readableJSON[i]["defid"].int!)
            do {
                try managedObjextContext.save()
            } catch {
                print("Could not save data \(error.localizedDescription)")
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "SlangDictionary")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension UIColor {
    static func candyGreen() -> UIColor {
        return UIColor(red: 67.0/255.0, green: 205.0/255.0, blue: 135.0/255.0, alpha: 1.0)
    }
}

