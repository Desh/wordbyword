//
//  DictionaryController.swift
//  SlangDictionary
//
//  Created by Денис Шаклеин on 25/02/2017.
//  Copyright © 2017 desage. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class DictionaryController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    let dictCellId = "dictCellId"
    var managedObjextContext:NSManagedObjectContext!
    let searchController = UISearchController(searchResultsController: nil)
    var slangwords = [SlangWord]()
    var filteredSlangwords = [SlangWord]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        managedObjextContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//        navigationController?.navigationBar.backgroundColor = 
        loadData()
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.barTintColor = UIColor(r: 238, g: 211, b: 140)

        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        let imageView = UIImageView(frame: self.view.frame)
        let image = UIImage(named: "bg")!
        imageView.image = image
        tableView.backgroundView = imageView
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Avenir-Heavy", size: 21)!]
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredSlangwords = slangwords.filter { word in
            return (word.word!.lowercased().range(of: searchText.lowercased()) != nil)        }
        
        tableView.reloadData()
    }
    
    func loadData(){
        let wordRequest:NSFetchRequest<SlangWord> = SlangWord.fetchRequest()
        
        do {
            slangwords  = try managedObjextContext.fetch(wordRequest)
            self.tableView.reloadData()
        }catch {
            print("Could not load data from database \(error.localizedDescription)")
        }
    }
    
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: dictCellId, for: indexPath)
        var slangword: String?
        if searchController.isActive && searchController.searchBar.text != "" {
            slangword = filteredSlangwords[indexPath.row].word
        } else {
            slangword = slangwords[indexPath.row].word
        }
        cell.textLabel?.text = slangword
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = .white
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredSlangwords.count
        }
        return slangwords.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWordDescription" {
            let wordVC = segue.destination as? WordDescriptionController
            if let indexPath = tableView.indexPathForSelectedRow?.row {
                print(slangwords[indexPath].word!)
                if searchController.isActive && searchController.searchBar.text != "" {
                    wordVC?.word = filteredSlangwords[indexPath].word!
                    wordVC?.definition = filteredSlangwords[indexPath].definition!
                    wordVC?.example = filteredSlangwords[indexPath].example!
                } else {
                    wordVC?.word = slangwords[indexPath].word!
                    wordVC?.definition = slangwords[indexPath].definition!
                    wordVC?.example = slangwords[indexPath].example!
                }

                
            }
        }
    }


}
extension DictionaryController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchText: searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}

extension DictionaryController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
}
