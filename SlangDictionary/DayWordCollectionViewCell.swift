//
//  DayWordCollectionViewCell.swift
//  SlangDictionary
//
//  Created by Денис Шаклеин on 08/03/2017.
//  Copyright © 2017 desage. All rights reserved.
//

import UIKit

class DayWordCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var wordTextView: UITextView!
    
    
}
