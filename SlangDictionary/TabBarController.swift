//
//  TabBarController.swift
//  SlangDictionary
//
//  Created by Денис Шаклеин on 21/02/2017.
//  Copyright © 2017 desage. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    var freshLaunch = true
    override func viewWillAppear(_ animated: Bool) {
        if freshLaunch == true {
            freshLaunch = false
            self.tabBarController?.selectedIndex = 2
        }
    }
    

}
