
import csv
import requests
import time
from selenium import webdriver, common

words_list = {}
driver = webdriver.Firefox()


SITE = 'http://www.urbandictionary.com'


def get_word(words):
    csvfile = open('words.csv', 'a')
    for word in words:
        t1 = time.time()
        word_info = requests.get('http://api.urbandictionary.com/v0/define?term={}'.format(word)).json()
        try:
            data = word_info['list'][0]
            wordsfile = csv.writer(csvfile)
            wordsfile.writerow([data['word'], data['definition'], data['example'],
                                data['thumbs_up'], data['thumbs_down'], data['defid']])
        except IndexError:
            pass
        print('one word time: {}'.format(t1 - time.time()))
    csvfile.close()


def parse_page(href):
    print(href)
    t1 = time.time()
    driver.get(href)
    elements = driver.find_elements_by_tag_name('a')
    # elements last element has nonetype href arg
    define_hrefs = [x.get_attribute('href') for x in elements[:-1] if '/define.php?term=' in x.get_attribute('href')]
    words_to_define = [x.split('term=')[1] for x in define_hrefs]
    print('loading info time: {}'.format(time.time() - t1))
    t1 = time.time()
    get_word(words_to_define)
    print('get words fun: {}'.format(time.time() - t1))
    next_page = driver.find_element_by_partial_link_text('Next').get_attribute('href')
    if SITE not in next_page:
        next_page = SITE+next_page
    parse_page(next_page)


def main():
    # first_href = "{}/browse.php?word={}".format(SITE, 'aa')
    first_href = 'http://www.urbandictionary.com/browse.php?word=aaful'
    try:
        parse_page(first_href)
    except common.exceptions.NoSuchElementException:
        print("work done")

main()