import twitter
import json
import time
from config import *
from multiprocessing.dummy import Pool as ThreadPool


def get_since_id(word, counter, time_start):
    sw = word.strip()
    word = "%20".join(word.split(" "))
    try:
        result = api.GetSearch(raw_query='q="{}"%20&result_type=recent&'
                                 'since=2017-03-01&count=200'.format(word))
    except (ValueError,):
        return (0, counter, time_start)
    except twitter.error.TwitterError as err:
        if err.message[0]['code']  == 32:
            return (0, counter, time_start)
        elif err.message[0]['code'] == 88:
            time.sleep(60 * 10)
            counter = 0
            time_start = time.time()
            result = api.GetSearch(raw_query='q="{}"%20&result_type=recent&'
                                 'since=2017-03-01&count=200'.format(word))

    since_id = len(result)
    if since_id < 1:
        return (0, counter, time_start)
    max_id = result[-1].id

    while(True):
        if counter == 180 and time.time() - time_start < 60 * 15:
            print("start_sleeping")
            time.sleep(60 * 15 - (time.time() - time_start) + 1)
            counter = 0
            time_start = time.time()
        try:
            result = api.GetSearch(raw_query='q="{}"%20&result_type=recent&since=2017-03-01&'
                                     'count=200&max_id={}&since_id={}'.format(word, max_id, since_id))
            cur_max = result[-1].id
            counter += 1
            if cur_max == max_id:
                break
            max_id = cur_max
            since_id += len(result)
            if since_id >= MAX_SINCE_ID:
                break
        except twitter.error.TwitterError:
            time.sleep(60 * 15)
            counter = 0
            time_start = time.time()

    return (since_id, counter, time_start)


def main():
    with open("words.json") as data_file:
        data = json.load(data_file)
        counter = 0
        time_start = time.time()
        with open("twitter_frequency.txt", 'r') as f:
            start_word_id = len(f.readlines()) - 1
        for i in range(start_word_id, NUMBER):
            word = data[i]["word"]
            result, counter, time_start = get_since_id(word, counter, time_start)
            with open("twitter_frequency.txt", "a") as f:
                f.write("{} : {}\n".format(word, result))
            print("{} : {}".format(word, result))
        # pool = ThreadPool(1)
        # RES = pool.map(get_since_id, data[:NUMBER])

NUMBER = 400
MAX_SINCE_ID = 5000
MORE_THAN_MAX_COUNT = 0
api = twitter.Api(consumer_key="cxtVSLJxF8Ugr2OZ6qkRKPWkN",
                  consumer_secret="7srfLZYxLiKqlc8ONNyHcMjN3EQ6SZQxDsDGnaDnSbFgrtmr6C",
                  access_token_key="1201630536-yzBYkux0h6DfUqlEZx6mbpIJgqETUeF9ttg8nmg",
                  access_token_secret="vnNAoxMa9aXmtvmYUg1DdivYCoRJqLwymCRujIAgj7tvX")
def qqq():
    return(api.GetSearch(raw_query='q="{hipster+love}"%20&result_type=recent&since=2017-03-01&'
                                     'count=10&max_id={}&since_id={}'))
res = api.GetSearch(raw_query='q="{hipster+love}"%20&result_type=recent&since=2017-03-01&'
                                     'count=10&max_id={}&since_id={}')
print([i.text for i in res])
exit()
main()

